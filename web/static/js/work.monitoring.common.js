/* 
 * By idkc
 */

!function($, moment) {
    var Common = function() {
        this.map = {};
    };
    Common.prototype = {
        constructor: Common,
        stringToDate: function(dateString, tipo) {
            var result;
            if (tipo == 'start')
                result = moment(dateString + " 00:00:00 -0500", "DD/MM/YYYY HH:mm:ss ZZ");
            else if (tipo == 'end')
                result = moment(dateString + " 23:59:59 -0500", "DD/MM/YYYY HH:mm:ss ZZ");
            return result;
        }
    };
    common = new Common();
}(window.jQuery, moment);

