var appkey = '2Ze1dz';
var token = 'admin';
var client;

// PRODUCT DETAILS

var productID = 1234;
var currentStock = 20;    

// CONNECT TO REALTIME

loadOrtcFactory(IbtRealTimeSJType, function (factory, error) {

     if (error != null) {
        console.log("Factory error: " + error.message);
     } else {                                
        client = factory.createClient();                
        client.setClusterUrl('http://ortc-developers.realtime.co/server/2.1/');

        client.onConnected = function() {
            document.getElementById('status').innerHTML = "connected";
        }

        client.connect(appkey, token);           
     }
});


// SEND PRODUCT PRICE UPDATE

function updatePrice(){
    var oldPrice = document.getElementById('oldPrice').value;
    var newPrice = document.getElementById('newPrice').value;
    var msg = '<b>before <em> $' + oldPrice + '</em></b><span>now $' + newPrice + '</span>';
    
    client.send('price-update:' + productID, msg);        
}



// SEND PRODUCT STOCK UPDATE

function decreaseStock(){                

    if(currentStock > 0) {
        currentStock--;    
    }            

    var msg = { stock: currentStock };

    client.send('stock-update:' + productID, JSON.stringify(msg));        
}

out_of_limit = []

function manage_color(element, value)
{
	var stock_min = 50;
	if(value < stock_min){

		if($.inArray(element, out_of_limit) == 0){			
		}
		else{
			//turns red
			out_of_limit.push(element);
			$( element ).removeClass('sliderGreen');
			$( element ).addClass('sliderRed');
			var title = 'Distribuidora Diaz'
			var button = '<a href="#" class="btn btn-info btn-supply">Programar Envio</a>'
			var message = 'El stock del producto con codigo ' + element + ' esta bajo su stock de seguridad recomendado. Se recomienda realizar la reposicion del producto en el agente: "Distribuidora Diaz". Dar click aqui para programar envio : ' + button;
			

			open_notification(title, message);			
			$('.btn-supply').click(function(e){
				e.preventDefault();
				$('#myModal').modal('show');
			});
			
		}
	}
	else{

		if($.inArray(element, out_of_limit) == 0){
			out_of_limit.pop(element);
			//turns green
			$( element ).removeClass('sliderRed');
			$( element ).addClass('sliderGreen');

		}
		else{

		}

	}
}


var set_product_stock = function(product_id, product_stock)
{
	$( ".id_material_" + product_id ).html( product_stock );
	$(".slider_" + product_id).slider({value: product_stock});
	manage_color(".slider_" + product_id ,product_stock);
}

var get_product_stock = function(product_id)
{
	var stock = $( ".id_material_" + product_id ).html();
	return parseFloat(stock);
}



