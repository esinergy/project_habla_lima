var appkey = '2Ze1dz';
var token = 'user';
var client;

// PRODUCT DETAILS

var products = ["090","100","110"];


// CONNECT TO REALTIME

loadOrtcFactory(IbtRealTimeSJType, function (factory, error) {
	if (error != null) {
	  alert("Factory error: " + error.message);
	} else {
	 		   		    
	    client = factory.createClient();
	    client.setClusterUrl('http://ortc-developers.realtime.co/server/2.1/');
	            
	    client.onConnected = function (c) {
	      $('#status').html('connected');

	      // SUBSCRIBE THE REALTIME CHANNELS TO RECEIVE UPDATES
	      // SUBSCRIBE TO EACH STOCK UPDATE CHANNEL
		  for (i in products)
		  {
			
			c.subscribe('price-update:' + products[i], true, priceUpdate);
			c.subscribe('stock-update:' + products[i], true, stockUpdate);
		  }
		  console.log('Connected');
	    };

	    client.connect(appkey, token);		   
	}
});    


// UPDATES THE PRODUCT PRICE

var priceUpdate = function(c, channel, msg) {
	updateAndBlink('#price', msg);		
}

out_of_limit = []


function manage_color(element, value)
{
	var stock_min = 50;
	if(value < stock_min){

		if($.inArray(element, out_of_limit) == 0){
			console.log('red1 - elem:' + element); 
		}
		else{
			//turns red
			out_of_limit.push(element);
			$( element ).removeClass('sliderGreen');
			$( element ).addClass('sliderRed');
			var title = 'Distribuidora Diaz'
			var button = '<a href="#" class="btn btn-info btn-supply">Programar Envio</a>'
			var message = 'El stock del producto con codigo ' + element + ' esta bajo su stock de seguridad recomendado. Se recomienda realizar la reposicion del producto en el agente: "Distribuidora Diaz". Dar click aqui para programar envio : ' + button;
			

			open_notification(title, message);			
			$('.btn-supply').click(function(e){
				e.preventDefault();
				$('#myModal').modal('show');
			});
			
		}
	}
	else{

		if($.inArray(element, out_of_limit) == 0){
			out_of_limit.pop(element);
			//turns green
			$( element ).removeClass('sliderRed');
			$( element ).addClass('sliderGreen');

		}
		else{

		}

	}
}


function open_notification(title, message)
{
		var unique_id = $.gritter.add({
			// (string | mandatory) the heading of the notification
			title: title,
			// (string | mandatory) the text inside the notification
			text: message,
			// (string | optional) the image to display on the left
			image: 'img/avatar.jpg',
			// (bool | optional) if you want it to fade out on its own or just sit there
			sticky: true,
			// (int | optional) the time you want it to be alive for before fading out
			time: '',
			// (string | optional) the class name you want to apply to that specific message
			class_name: 'my-sticky-class'
		});
}

// UPDATES THE PRODUCT STOCK

var set_product_stock = function(product_id, product_stock)
{
	$( ".id_material_" + product_id ).html( product_stock );
	$(".slider_" + product_id).slider({value: product_stock});
	manage_color(".slider_" + product_id ,product_stock);
}

var stockUpdate = function(c, channel, msg) {
	var channel_name = "stock-update:";
	var product_id = channel.substring(channel_name.length); 

	//NEW

	stockUpdate = JSON.parse(msg);
	//updateAndBlink('#stock', stockUpdate.stock)
	set_product_stock(product_id, stockUpdate.stock)
	//$('.circleChart').val(stockUpdate.stock);
}


// UPDATE AND BLINK UI

var updateAndBlink = function(selector, value){    	    	
	$(selector).css('color', 'red');
	$(selector).fadeOut('slow', function(){			
		$(selector).html(value);
		$(selector).css('color', 'black');			
	    $(this).fadeIn('slow');
	});
	
}